// import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './src/screens/Loginscreen'
import ResetPassword from './src/screens/Resetpassword'
import SplashScreen from './src/screens/Splashscreen'
import RegistrationScreen from './src/screens/Registerscreen'
import LocalBusinessDirectory from './src/screens/LocalBusinessDirectory'
import CreatePostScreen from './src/screens/CreatePostScreen'
import AppHeader from './src/components/AppHeader'
import AbroadServiceScreen from './src/screens/AbroadServicescreen'
import { navigationRef } from './src/RootNavigation';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{headerShown: false, }}>
        {/* <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="login" component={LoginScreen} />
        <Stack.Screen name="resetpassword" component={ResetPassword} />
        <Stack.Screen name="register" component={RegistrationScreen} />
        <Stack.Screen name="localBusinessDirectory" component={LocalBusinessDirectory} /> 
        <Stack.Screen name="CreatePost" component={CreatePostScreen} />
        <Stack.Screen name="apphedr" component={AppHeader} />  */}
        <Stack.Screen name="abroadservice" component={AbroadServiceScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;
