import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

    render() {
      
    return (
        <View style={styles.appHedr}>
            <TouchableOpacity
            onPress={()=>this.props.navigation.goBack()}>
            <Image
                style={styles.image_size}
                source={require('../assest/iconss/icon_arrow_left.png')}
            />
            </TouchableOpacity>
            <View style={styles.loctn_txt_gap}>
                <Image
                style={styles.image_size}
                source={require('../assest/iconss/locations.jpeg')}
            />
                <Text style={styles.txt}>Sharjah</Text>
            </View>
            
      </View>
    );
  }
}

const styles = StyleSheet.create({
    appHedr: {
        paddingVertical: 20,
        width:"100%",
        paddingHorizontal:10,
        flexDirection: "row",
        justifyContent: "space-between",
    },

   image_size: {
        height: 15,
       width:15
    },
   
    txt: {
        fontSize: 12,
        right: 100,
        left: 5,
        color:"black"
    },
    
    locatn_icon: {
        height: 5,
        width:8
    },
    
    loctn_txt_gap: {
        flexDirection: 'row',
        right:50
    }
    
})

export default AppHeader;
