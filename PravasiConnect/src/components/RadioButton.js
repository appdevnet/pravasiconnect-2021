import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

export default class RadioButton extends Component {
	state = {
		value: null,
	};

	render() {
		const { PROP } = this.props;
		const { value } = this.state;

		return (
            <View style={styles.radioView}>
				{PROP.map(res => {
					return (
						<View key={res.key} style={styles.container}>
							
							<TouchableOpacity
								style={styles.radioCircle}
								onPress={() => {
									this.setState({
										value: res.key,
									});
								}}>
                                  {value === res.key && <View style={styles.selectedRb} />}
                            </TouchableOpacity>
                            <Text style={styles.radioText}>{res.text}</Text>
						</View>
					);
				})}
                {/* <Text> Selected: {this.state.value} </Text> */}
			</View>
		);
	}
}

const styles = StyleSheet.create({
    radioView: {
        flexDirection: "row",
        width: "100%"
    },

	container: {
        marginBottom: 15,
        flexDirection: "row",
        paddingHorizontal:10
        
	},
    radioText: {
        paddingHorizontal:5,
        fontSize: 15,
        color: '#000',
        fontWeight: '700'
    },
    radioCircle: {

        marginLeft:10,
        paddingHorizontal:5,
		height: 15,
		width: 15,
		borderRadius: 100,
		borderWidth: 2,
		borderColor: 'black',
	},
	selectedRb: {
		width: 12,
		height: 12,
		borderRadius: 100,
		left: -5,
		backgroundColor: '#3740ff',
    },
    result: {
        marginTop: 20,
        color: 'white',
        fontWeight: '600',
        backgroundColor: '#F3FBFE',
    },
});