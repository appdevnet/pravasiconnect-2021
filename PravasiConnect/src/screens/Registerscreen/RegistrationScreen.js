import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from "react-native";

class RegistrationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView>
        <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <View style={styles.headinView}>
          <Text style={styles.registerHeading}>Register</Text>
          </View>

          <View style={styles.container}>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Choose Residing Country"
          placeholderTextColor="#A5A5A5"
          
        />
            </View>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Your Full name"
          placeholderTextColor="#A5A5A5"
          
        />
            </View>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Mobile Number"
          placeholderTextColor="#A5A5A5"
          
        />
            </View>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email Address"
          placeholderTextColor="#A5A5A5"
          
        />
            </View>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#A5A5A5"
          
        />
            </View>

            <View>
              <Text style={styles.trmsTxt}>Accept our Terms & Conditions</Text>
            </View>
            
            <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>REGISTER NOW</Text>
            </TouchableOpacity>
            
            <View style={styles.socialtxt}>
              <Text>Already have an account ? <TouchableOpacity
              onPress={() => navigation.navigate('login')}>
                <Text style={styles.registrbtn}>LOGIN NOW</Text></TouchableOpacity></Text>
          </View>

          </View>
          </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: 10,
    marginTop: 70,
    marginBottom: 30,
    marginHorizontal: 10
  },

  registerHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    
  },

  inputView: {
    backgroundColor: "#e0dfe6",
    borderRadius: 12,
    width: "70%",
    height: 50,
    marginBottom: 30,

  },
 
  TextInput: {
    height: 50,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  trmsTxt: {
    color: "#000"
  },

  loginBtn: {
    width: "80%",
    borderRadius: 20,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: 40,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});

export default RegistrationScreen;
