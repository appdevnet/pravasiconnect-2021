import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView
} from "react-native";
import RadioButton from '../../components/RadioButton'
import SwitchButton from '../../components/SwitchButton'


const PROP = [
  {
    key: 'Call',
    text: 'Call'
  },
  {
    key: 'Message',
    text: 'Message'
  }
]

class LocalBusinessDirectory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switch1Value: false,
    };
  }

  toggleSwitch1 = (value) => {
      this.setState({switch1Value: value})
      console.log('Switch 1 is: ' + value)
   }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
        showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled">
          
          <View style={styles.headinView}>
          <Text style={styles.LocalBusinessDirectory}>Local Business Directory</Text>
          </View>

          <View style={styles.container}>

            <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Business Name"
          placeholderTextColor="#003f5c"
          
        />
            </View>

          <View style={styles.descriotion}>
        <TextInput
          style={styles.TextInput}
          placeholder="Description"
          placeholderTextColor="#003f5c"
          
        />
            </View>
            
          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="City"
          placeholderTextColor="#003f5c"
          
        />
            </View>
            
          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Address"
          placeholderTextColor="#003f5c"
          
        />
            </View>  


          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Person Number"
          placeholderTextColor="#003f5c"
          
        />
            </View>
            

          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Email "
          placeholderTextColor="#003f5c"
          
        />
            </View>
            

          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Contact Number "
          placeholderTextColor="#003f5c"
          
        />
            </View> 
            
          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Upload Image "
          placeholderTextColor="#003f5c"
          
        />
            </View>  

          </View>

          <View style={styles.ContactheadinView}>
          <Text style={styles.ContactPerson}>Contact Method</Text>
          </View>

          <View>
            <RadioButton PROP={ PROP }/>
          </View>

          <View style={styles.PrivacyheadinView}>
          <Text style={styles.Privacy}>Privacy</Text>
          </View>

          <View style={styles.switchView}>
            <Text style={styles.txtSwitch}>Show Phone Number in Profile</Text>
            <SwitchButton
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value = {this.state.switch1Value}/>
          </View>

          <View style={styles.switchView}>
            <Text style={styles.txtSwitch}>Show Email Address in Profile </Text>
            <SwitchButton
            toggleSwitch1 = {this.toggleSwitch1}
            switch1Value = {this.state.switch1Value}/>
          </View>

          <TouchableOpacity>
          <View style={styles.lcl_bsns_d_regstr_Btn}>
            <Text style={styles.lcl_bsns_d_regstr_txt}>REGISTER NOW</Text>
            </View>
          </TouchableOpacity>



        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: 10,
    marginTop: 70,
    marginBottom: 30,
    marginHorizontal: 10
  },

  LocalBusinessDirectory: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,  
  },

  descriotion: {
    backgroundColor: "#e0dfe6",
    borderRadius: 12,
    width: "80%",
    height: 100,
    marginBottom: 20,
  },

  inputView: {
    backgroundColor: "#e0dfe6",
    borderRadius: 12,
    width: "80%",
    height: 50,
    marginBottom: 20,
  },
 
  TextInput: {
    height: 50,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  ContactheadinView: {
    paddingHorizontal: 10,
    marginTop: 10,
    marginBottom: 30,
    marginHorizontal: 10
  },

  ContactPerson: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  switchView: {
    flexDirection: "row",
    paddingHorizontal: 10,
    justifyContent:"space-around",
    marginBottom:30
  },

  PrivacyheadinView: {
    paddingHorizontal: 10,
    marginTop: 10,
    marginBottom: 30,
    marginHorizontal: 10
  },

  Privacy: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  txtSwitch: {
    paddingHorizontal: 15,
    left:10
  },

  lcl_bsns_d_regstr_Btn: {
    width: "100%",
    height: 70,
    backgroundColor:"#070545"
  },

  lcl_bsns_d_regstr_txt: {
    fontSize: 20,
    fontWeight: "bold",
    alignSelf: "center",
    paddingVertical:"6%",
    color:"white"
  }

});

export default LocalBusinessDirectory;