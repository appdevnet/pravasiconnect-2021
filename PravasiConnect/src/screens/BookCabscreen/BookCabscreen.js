import React, { Component } from 'react';
import { View, Text,SafeAreaView,StyleSheet,Image,ScrollView } from 'react-native';
import { require } from 'yargs';
import taxi from '../../assets/delivery_taxi_13.jpg';

class BookCabscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView>
        <ScrollView>
      <View>
        <Text style={styles.heading}> Book a Cab </Text>
      </View>
      <View>
          <Image source={taxi} style={styles.image}></Image>
      </View>
      <View>
          <Text style={styles.content}>Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
      </View>
      </ScrollView>
      </SafeAreaView>
    );
  }
}

export default BookCabscreen;


const styles = StyleSheet.create({
    
    heading:{
        fontSize:20,
        fontWeight:'bold',
        marginTop:70,
        marginLeft:20,
    },

    image:{
      width:'90%%',
      height:250,
      padding:20,
      marginTop:15,
      marginLeft:20,
      justifyContent:'center',
      alignItems:'center',
    },

    content:{
      alignContent:'center',
      justifyContent:'center',
      padding:20,
    },
})