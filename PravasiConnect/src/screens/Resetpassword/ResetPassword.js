import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView
} from "react-native";

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView>
        <View style={styles.headinView}>
          <Text style={styles.resetHeading}>Reset Password</Text>
        </View>

        <View style={styles.container}>
          <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email Address"
          placeholderTextColor="#A5A5A5"
          
        />
          </View>
          
          <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>RESET PASSWORD</Text>
          </TouchableOpacity>
          

        <View style={styles.socialtxt}>
            <Text>Back to <TouchableOpacity
            onPress={() => navigation.navigate('login')}>
              <Text style={styles.registrbtn}>SIGN IN</Text></TouchableOpacity></Text>
          </View>

        
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headinView: {
    paddingHorizontal: 10,
    marginTop: 70,
    marginBottom: 30,
    marginHorizontal: 10
  },

  resetHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 80,
    
  },

  inputView: {
    backgroundColor: "#e0dfe6",
    borderRadius: 12,
    width: "70%",
    height: 50,
    marginBottom: 30,

  },
 
  TextInput: {
    height: 50,
    paddingHorizontal: 10,
    marginHorizontal: 10,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 20,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: 300,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});
export default ResetPassword;
