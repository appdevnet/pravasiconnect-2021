import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Platform,
  FlatList,
  Alert
} from "react-native";
import AppHeader from '../../components/AppHeader'


class AbroadServiceScreen extends Component {
  constructor(props) {
    super(props);
      this.state = {
        GridListItems: [
        { key: "Jobs" },
        { key: "Rooms" },
        { key: "Sales/Rent" },
        { key: "Restaurants" },
        { key: "Exchange" },
        { key: "Saloons" },
        { key: "Taxi" },
        { key: "Shopping Mall" },
        { key: "Supermarket" },
      ]
    };
  }
    
    GetGridViewItem(item) {
    Alert.alert(item);
  }

  render() {
    return (
      <SafeAreaView style={styles.complete_view}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
                
            
            <View style={{position:"relative",marginTop:0,}}>
                <Image
                    style={{height:220,width:"100%"}}
                    source={{uri:'https://assets.sentinelassam.com/h-upload/2021/04/22/220277-council-for-insurance-ombudsmen-cio.jfif'}}
                />
            </View>
                
            <View style={styles.headinView}>
          <Text style={styles.headnTxt}>Available Services in UAE</Text>
                </View>
                
            <View style={styles.container}>
            <FlatList 
            showsVerticalScrollIndicator={false}
            data={ this.state.GridListItems }
            renderItem={ ({item}) =>
                <View style={styles.GridViewContainer}>
                    <Image style={{ height: 30, width: 20 }}
                        source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa-rhdF6h5xQDgW1c3hPENsrDoyMUJVAjlDC8uktyw5vP7EKmC9CTdCaS5HhSywBYy4lw&usqp=CAU'}}
                    />
               <Text style={styles.GridViewTextLayout} > {item.key} </Text>
              </View> }
            numColumns={3}
         />
       </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    complete_view: {
      flex: 1,
      backgroundColor: "white",
    },

    headinView: {
      paddingHorizontal: 10,
      marginTop: 50,
      marginBottom: 30,
      marginHorizontal: 25
  },

  headnTxt: {
    fontWeight: "bold",
    fontSize: 15,
    color: "black"
    },
  
  container: {
      width: "100%",
      height: 500,
      justifyContent: "center",
      alignItems: 'center',
      backgroundColor: "#fff"
  },
  
  GridViewContainer: {
      marginTop: 20,
      marginBottom:20,
      justifyContent: 'center',
      alignItems: 'center',
      height: 120,
      width:100,
      margin: 5,
      borderColor: "grey",
      borderWidth: 1,
      borderRadius:12,
      backgroundColor:"#fff"
},
GridViewTextLayout: {
      fontSize: 10,
      color:"#0e0440",
      fontWeight: "200",
      justifyContent: 'center',
      padding: 10,
 }
    
})

export default AbroadServiceScreen;
