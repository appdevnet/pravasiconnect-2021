import React, { Component } from 'react';
import { View, Text,SafeAreaView,StyleSheet } from 'react-native';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView>
      <View>
        <Text style={styles.title}>PRAVASI</Text>
        <Text style={styles.connect}>CONNECT</Text>
        <Text style={styles.tap_to_login} onPress={()=>this.props.navigation.navigate('MapScreen')}>Tap to Login</Text>
      </View>
      </SafeAreaView>
    );
  }
}

export default SplashScreen;


const styles = StyleSheet.create({

  container:{
    flex:1,
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center',


},
title:{
    textAlign: 'center',
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop:300,
    
},

tap_to_login:{
  textAlign:'center',
  color:'black',
  fontSize:15,
  marginTop:380,
},
connect:{
  textAlign:'center',
  fontSize:20,
}

})