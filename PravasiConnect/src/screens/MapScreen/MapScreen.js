import React, { Component } from 'react';
import { View, Text,SafeAreaView,TouchableOpacity,StyleSheet,Modal } from 'react-native';
import SearchBar from 'react-native-search-bar';
import MapView from 'react-native-maps';  
import { Marker } from 'react-native-maps';  
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

class MapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  
  
  

  render() {
    return (
      

        <SafeAreaView> 

         {/* <GooglePlacesAutocomplete
      
      placeholder='Search'
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data, details);
      }}
      query={{
        key: 'YOUR API KEY',
        language: 'en',
      }}

        />  */}

        
          
        <View>
        <SearchBar
        placeholder={'Seach Your Location'}>
         </SearchBar>
        </View>
       

         <View style={styles.MainContainer}>
         <MapView  
          style={styles.mapStyle}  
          showsUserLocation={false}  
          zoomEnabled={true}  
          zoomControlEnabled={false}  
          initialRegion={{  
            latitude: 28.579660,   
            longitude: 77.321110,  
            latitudeDelta: 0.0922,  
            longitudeDelta: 0.0421,  
          }}>  
  
          <Marker  
            coordinate={{ latitude: 28.579660, longitude: 77.321110 }}  
            title={"location"}  
            description={"My Location"}  
          />  
        </MapView>

         </View> 
      
      <View>
      <TouchableOpacity style={styles.locationBtn} onPress={()=> this.props.navigation.navigate('login')}>
        <Text style={styles.locationText}>SET YOUR LOCATION</Text>
      </TouchableOpacity>
      </View>
      </SafeAreaView>
    );
  }
}

export default MapScreen;


const styles = StyleSheet.create({
 
  locationBtn: {
    width: "80%",
    borderRadius: 60,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 600,
    backgroundColor: "#070545",
    
  },

  locationText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  MainContainer: {  
    position: 'absolute',  
    top: 0,  
    left: 0,  
    right: 0,  
    bottom: 0,  
    alignItems: 'center',  
    justifyContent: 'center',
      
  },  
  mapStyle: {  
    position: 'relative',  
    top: 0,  
    left: 0,  
    right: 0,  
    bottom: 0,  
  },  



})