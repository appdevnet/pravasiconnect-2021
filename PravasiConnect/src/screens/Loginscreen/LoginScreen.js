import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import AppHeader from '../../components/AppHeader'




class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView style={styles.complete_view}>
        <AppHeader {...this.props}/>
        <View style={styles.headinView}>
          <Text style={styles.loginHeading}>Login</Text>
        </View>

      <View style={styles.container}>

        <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email Address"
          placeholderTextColor="#A5A5A5"
          // onChangeText={(email) => setEmail(email)}
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#A5A5A5"
          secureTextEntry={true}
          // onChangeText={(password) => setPassword(password)}
            />
           </View> 
          
          <TouchableOpacity
          onPress={() => navigation.navigate('resetpassword')}>
            
              <Text style={styles.forgot_button}>Forgot your Password?</Text>
            
      </TouchableOpacity>
 
          <TouchableOpacity style={styles.loginBtn}
          onPress={() => navigation.navigate('localBusinessDirectory')}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
          
          
          <View style={styles.socialtxt}>
            <Text>Or login with Social Media</Text>
          </View>

          <View style={styles.socialtxt}>
            <Text>Don't have an account ? <TouchableOpacity
            onPress={() => navigation.navigate('register')}>
              <Text style={styles.registrbtn}>REGISTER NOW</Text></TouchableOpacity></Text>
          </View>
        
        </View>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  complete_view: {
    flex: 1,
    backgroundColor: "white",

  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
    
  },

  headinView: {
    paddingHorizontal: 10,
    marginTop: 40,
    marginBottom: 30,
    marginHorizontal: 10

  },

  loginHeading: {
    fontWeight: "bold",
    fontSize: 20,
    color: "black"
  },

 
  inputView: {
    backgroundColor: "#f3f2f7",
    borderRadius: 12,
    width: "80%",
    height: 50,
    marginBottom: 20,
  },
 
  TextInput: {
    height: 50,
    paddingHorizontal: 10,
    marginHorizontal: 10,
 
  },

  forgot_button: {
    marginTop: 10,
    marginBottom: 20,
    left:"20%"
      
  },
 
  loginBtn: {
    width: "80%",
    borderRadius: 20,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    marginTop:10,
    backgroundColor: "#070545",
    
  },

  loginText: {
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 17,
    fontWeight: "bold",
    color: "white"
  },

  socialtxt: {
    paddingTop: 140,
    fontSize:15,
  },

  registrbtn: {
    color: "#070545",
    fontSize: 15,
    top:3,
    fontWeight: "bold"
    
  }
});

export default LoginScreen;
