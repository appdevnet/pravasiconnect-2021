const INITAL_STATE = {
    isLoggedIn: 'false',
}

export default (state = INITAL_STATE, action) => {
    switch (action.type) {
        case 'ISLOGGEDIN':
            return {...state, isLoggedIn: action.payload};
        case 'ISCONNECTED':
            return {...state, isConnected: action.payload};
        default :
            return state
    }}